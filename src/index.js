const { app, BrowserWindow, Menu, Tray } = require('electron');
const path = require('path');
const envVars = process.env;

let chatgptProxy = envVars?.CHATGPT_PROXY ? envVars?.CHATGPT_PROXY : "127.0.0.1:7890"

//配置代理
app.commandLine.appendSwitch('proxy-server', chatgptProxy);

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) {
  app.quit();
}

let tray = null;
let mainWindow = null;

const createWindow = () => {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 1200,
    height: 800,
    icon: path.join(__dirname, 'chatgpt.png'),
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
    },
  });

  // and load the index.html of the app.
  //mainWindow.loadFile(path.join(__dirname, 'index.html'));
  mainWindow.loadURL("https://chatgpt.com/")

  // Open the DevTools.
  // mainWindow.webContents.openDevTools();
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
  createWindow()

  // 创建系统托盘图标
  tray = new Tray(path.join(__dirname, 'chatgpt.png'));

  // 创建上下文菜单
  const contextMenu = Menu.buildFromTemplate([
    { label: '显示应用程序', click: () => { mainWindow.show(); } },
    { label: '退出', click: () => { app.quit(); } }
  ]);

  // 将上下文菜单分配给托盘图标
  tray.setToolTip('Your App');
  tray.setContextMenu(contextMenu);

  // 托盘图标被点击时的操作
  tray.on('click', () => {
    // 点击图标时执行的逻辑
  });
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
